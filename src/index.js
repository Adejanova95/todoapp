import React from "react"
import { render } from "react-dom"
import { TodoApp } from "./common/todo-app"

require("./styles/main.scss")

export const App = () => <TodoApp />

render(<App />, document.getElementById("app"))
