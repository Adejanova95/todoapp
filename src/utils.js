const axios = require("axios")
import { AuthStr } from "./constants"

export const requestData = (url, method, data = {}) => {
	return axios({
		url: url,
		method: method,
		data: data,
		headers: { Authorization: AuthStr, Accept: "application/json" },
	})
}

export const handleErrorBehaviour = (instance, error) => {
	instance.setState({
		errorMessage: error.response.data.message,
		isRequestError: true,
		isLoading: false,
	})
}

export const handleLoadingBehaviour = instance => {
	instance.setState({ isLoading: true, isRequestError: false })
}
