import React, { Component } from "react"
import TodoForm from "../todo-form"
import TodoList from "../todo-list"
import styles from "./todo-app.scss"
import { apiUrl, proxyurl } from "../../constants"

import {
	handleErrorBehaviour,
	handleLoadingBehaviour,
	requestData,
} from "../../utils"

export class TodoApp extends Component {
	constructor(props) {
		super(props)
		this.state = {
			data: [],
			isLoading: false,
			isRequestError: false,
			errorMessage: null,
		}

		this.addTodo = this.addTodo.bind(this)
		this.removeTodo = this.removeTodo.bind(this)
	}

	componentDidMount() {
		handleLoadingBehaviour(this)
		requestData(proxyurl + apiUrl, "get")
			.then(response => {
				const responseData = response.data.data.map(element => element.data)
				this.setState({
					data: responseData,
					isLoading: false,
					isRequestError: false,
				})
			})
			.catch(error => handleErrorBehaviour(this, error))
	}

	addTodo(titleValue, descriptionValue) {
		const todo = {
			title: titleValue,
			description: descriptionValue,
		}
		const { data } = this.state
		handleLoadingBehaviour(this)
		requestData(proxyurl + apiUrl, "post", todo)
			.then(res => {
				this.setState({
					data: data.concat({ attributes: res.data, id: res.data.id }),
					isLoading: false,
					isRequestError: false,
				})
			})
			.catch(error => handleErrorBehaviour(this, error))
	}

	removeTodo(id) {
		const { data } = this.state
		handleLoadingBehaviour(this)
		requestData(`${proxyurl + apiUrl}/${id}`, "delete")
			.then(() => {
				this.setState({
					data: data.filter(todo => {
						if (todo.id !== id) {
							return todo
						}
					}),
					isLoading: false,
					isRequestError: false,
				})
			})
			.catch(error => handleErrorBehaviour(this, error))
	}

	render() {
		const { data, isLoading, isRequestError, errorMessage } = this.state
		return (
			<div className={styles.root}>
				<TodoForm
					addTodo={this.addTodo}
					isTodoItemsLoading={isLoading}
					errorMessage={errorMessage}
					isRequestError={isRequestError}
				/>
				<TodoList todos={data} removeTodo={this.removeTodo} />
			</div>
		)
	}
}
