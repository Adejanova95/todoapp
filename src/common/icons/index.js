import React, { Component } from "react"
import styles from "./icons.scss"

export const Close = ({ className }) => (
	<svg className={className} viewBox="0 0 23.71 23.71">
		<path
			fill="currentColor"
			d="M23.71.71L23 0 11.85 11.15.71 0 0 .71l11.15 11.14L0 23l.71.71 11.14-11.15L23 23.71l.71-.71-11.15-11.15L23.71.71z"
		/>
	</svg>
)

const icons = {
	close: [Close],
}

export default class Icon extends Component {
	render() {
		const { type, onClick = () => {}, className } = this.props
		const [Icon] = icons[type]

		return (
			<span onClick={onClick} className={className}>
				{Icon && <Icon className={styles.svgSize} />}
			</span>
		)
	}
}
