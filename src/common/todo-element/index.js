import React, { Component } from "react"
import styles from "./todo-element.scss"
import Icon from "../icons"
import SingleTodoView from "../single-todo-view"

export default class Todo extends Component {
	constructor(props) {
		super(props)
		this.state = { isOpenSingleTodo: false }
		this.toggleSingleTask = this.toggleSingleTask.bind(this)
	}

	toggleSingleTask() {
		this.setState(state => ({
			isOpenSingleTodo: !state.isOpenSingleTodo,
		}))
	}

	removeTodoItem(e) {
		const { todo, removeTodo } = this.props
		e.stopPropagation()
		removeTodo(todo.id)
	}

	render() {
		const { todo } = this.props
		const { title, description } = todo.attributes
		const { isOpenSingleTodo } = this.state

		return (
			<React.Fragment>
				<div className={styles.root} onClick={this.toggleSingleTask}>
					<div className={styles.header}>
						<Icon
							type="close"
							className={styles.closeIcon}
							onClick={e => {
								this.removeTodoItem(e)
							}}
						/>
						<p className={styles.title}>{title}</p>
					</div>
					<p className={styles.description}>{description}</p>
				</div>
				{isOpenSingleTodo && (
					<SingleTodoView
						{...this.props}
						toggleSingleTask={this.toggleSingleTask}
					/>
				)}
			</React.Fragment>
		)
	}
}
