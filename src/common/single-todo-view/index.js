import React, { Component } from "react"
import styles from "./single-todo-view.scss"
import CommentForm from "../comment-form"
import Icon from "../icons"
import { apiUrl, proxyurl } from "../../constants"

import {
	requestData,
	handleErrorBehaviour,
	handleLoadingBehaviour,
} from "../../utils"

export default class SingleTodoView extends Component {
	constructor(props) {
		super(props)
		this.state = {
			data: [],
			isLoading: false,
			isRequestError: false,
			errorMessage: null,
		}
		this.addComment = this.addComment.bind(this)
	}

	componentDidMount() {
		handleLoadingBehaviour(this)
		requestData(`${proxyurl + apiUrl}/${this.props.todo.id}/comments`, "get")
			.then(response => {
				const data = response.data.data.map(element => element.data)
				this.setState({
					data: data,
					isLoading: false,
					isRequestError: false,
				})
			})
			.catch(error => handleErrorBehaviour(this, error))
	}

	addComment(textValue) {
		const todoComment = {
			text: textValue,
		}
		const { data } = this.state
		handleLoadingBehaviour(this)
		requestData(
			`${proxyurl + apiUrl}/${this.props.todo.id}/comments`,
			"post",
			todoComment,
		)
			.then(res => {
				this.setState({
					data: data.concat({ attributes: res.data }),
					isLoading: false,
					isRequestError: false,
				})
			})
			.catch(error => handleErrorBehaviour(this, error))
	}

	render() {
		const { data, isLoading, isRequestError, errorMessage } = this.state
		const { todo, toggleSingleTask } = this.props
		const { title, description } = todo.attributes
		const taskComments = data.map(element => element.attributes)
		const loadingText = "Loading..."

		return (
			<div className={styles.modal}>
				<div className={styles.modalContent}>
					<div className={styles.modalHeader}>
						<Icon
							type="close"
							onClick={toggleSingleTask}
							className={styles.closeButton}
						/>
						<h2>{title}</h2>
					</div>
					<p>{description}</p>
					<h3>Comments</h3>
					{isLoading && <p className={styles.loadingText}>{loadingText}</p>}
					{isRequestError && <p>{errorMessage}</p>}
					<CommentForm addComment={this.addComment} />
					{taskComments.map((element, id) => (
						<p key={id + element.text}>{element.text}</p>
					))}
				</div>
			</div>
		)
	}
}
