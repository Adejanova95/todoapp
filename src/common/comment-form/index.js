import React, { Component } from "react"
import styles from "./comment-form.scss"

export default class CommentForm extends Component {
	constructor(props) {
		super(props)
		this.state = { inputValue: "" }

		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
	}

	handleChange(e) {
		this.setState({ inputValue: e.target.value })
	}

	handleSubmit(e) {
		const { inputValue } = this.state
		const { addComment } = this.props
		e.preventDefault()
		addComment(inputValue)
		this.setState({ inputValue: "" })
	}

	render() {
		const { inputValue } = this.state
		const commentButtonText = "Create"
		return (
			<form className={styles.root} onSubmit={this.handleSubmit}>
				<input value={inputValue} onChange={this.handleChange} />
				<button>{commentButtonText}</button>
			</form>
		)
	}
}
