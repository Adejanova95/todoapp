import React from "react"
import Todo from "../todo-element"

const TodoList = ({ todos, removeTodo }) => {
	const todoNode = todos.map(todo => {
		return <Todo todo={todo} key={todo.id} removeTodo={removeTodo} />
	})
	const todoListText = "TODO List"
	return (
		<React.Fragment>
			<h3>{todoListText}</h3>
			<div>{todoNode}</div>
		</React.Fragment>
	)
}

export default TodoList
