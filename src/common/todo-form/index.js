import React, { Component } from "react"
import styles from "./todo-form.scss"

export default class TodoForm extends Component {
	constructor(props) {
		super(props)
		this.state = {
			textValue: "",
			descriptionValue: "",
		}

		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
	}

	handleChange(event) {
		const target = event.target
		const value = target.value
		const name = target.name
		this.setState({
			[name]: value,
		})
	}

	handleSubmit(e) {
		const { textValue, descriptionValue } = this.state
		const { addTodo } = this.props
		e.preventDefault()
		addTodo(textValue, descriptionValue)
		this.setState({ textValue: "", descriptionValue: "" })
	}

	render() {
		const { textValue, descriptionValue } = this.state
		const { isTodoItemsLoading, isRequestError, errorMessage } = this.props
		const formTitleText = "Title:"
		const formDescriptionText = "Description:"
		const formButtonText = "Create"
		const loadingText = "Please wait ..."
		return (
			<div>
				<form onSubmit={this.handleSubmit}>
					<h2>{formTitleText}</h2>
					<input
						name="textValue"
						value={textValue}
						onChange={this.handleChange}
					/>
					<h3>{formDescriptionText}</h3>
					<input
						name="descriptionValue"
						value={descriptionValue}
						onChange={this.handleChange}
					/>
					<button className={styles.button}>{formButtonText}</button>
				</form>
				{isTodoItemsLoading && (
					<p className={styles.loadingText}>{loadingText}</p>
				)}
				{isRequestError && <p>{errorMessage}</p>}
			</div>
		)
	}
}
