const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const autoprefixer = require("autoprefixer")

module.exports = {
	entry: './src/index.js',
	output: {
		path: path.resolve('dist'),
		filename: 'bundle.js',
	},
	resolve: {
			extensions: ['.js', '.jsx'],
	},
	module: {
		loaders: [
			{
				test: /\.js?$/,
				exclude: /(node_modules)/,
				loader: 'babel-loader',
				query: {
					presets: ['react', 'es2015', 'stage-3'],
				},
			},
			{
				test: /\.scss$/,
				use: [
					"style-loader",
					{
						loader: `css-loader`,
						options: {
							importLoaders: 1,
							localIdentName: `[name]__[local]--[hash:base64:5]`,
							sourceMap: true,
						},
					},
					{
						loader: "postcss-loader",
						options: {
							sourceMap: true,
							plugins: [autoprefixer()],
						},
					},
					{
						loader: "sass-loader",
						options: {
							sourceMap: true,
							includePaths: [
								path.resolve("src/styles"),
								path.resolve("src/components"),
								path.resolve("node_modules"),
							],
						},
					},
				],
				include: path.resolve("src"),
			},
		],
	},
		plugins: [
			new HtmlWebpackPlugin({
				template: './src/index.html',
				filename: 'index.html',
				inject: 'body',
			}),
		],
}
