#Simple task management system (i.e. to-do list) created with pure React and back-end endpoints.

To start development run ---> "npm install" and then "npm start"

There was problems with CORS, that's why there is added url - "https://cors-anywhere.herokuapp.com/"
in front of backend endpoints. Maybe that's why requests are so slow, maybe..

Also project is using CSS modules , which means that classNames are local , not global ,
and css className output name is generated using file name and className , for example,
we have file "todo-element" and <p className={styles.title} /> , className output will be todo-element__title.

If project will grow, it's very useful to use redux together with react (for state management) and
create atomic data structure.

Also webpack is configured only for development build. But it can be fixed (including production build configuration too)

If there is a lot of tasks (big list) , then it's useful would be implement lazy loading.
